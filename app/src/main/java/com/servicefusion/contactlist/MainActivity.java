package com.servicefusion.contactlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.servicefusion.contactlist.entities.Contact;
import com.servicefusion.contactlist.fragments.ContactDetailFragment;
import com.servicefusion.contactlist.fragments.ContactListFragment;
import com.servicefusion.contactlist.fragments.NewContactFragment;

import org.parceler.Parcels;

public class MainActivity extends FragmentActivity implements ContactListFragment.OnListFragmentInteractionListener, View.OnClickListener {

    private ContactDetailFragment mContactDetailFragment;
    private ContactListFragment mContactListFragment;
    private FloatingActionButton mAddContactBtn;
    private DatabaseReference mDatabase;
    private NewContactFragment mNewContactFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        setContentView(R.layout.activity_main);

        mAddContactBtn = (FloatingActionButton) this.findViewById(R.id.addContactBtn);
        mAddContactBtn.setOnClickListener(this);


//        mContactListFragment = new ContactListFragment();
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.replace(R.id.mainFrame, mContactListFragment, ContactListFragment.class.getName());
//        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(Contact contact) {
        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) {

            mContactDetailFragment = ContactDetailFragment.newInstance(contact);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.addToBackStack(ContactListFragment.class.getName());
            ft.replace(R.id.mainFrame, mContactDetailFragment, ContactDetailFragment.class.getName());
            ft.commit();

        } else {
            Intent intent = new Intent(this, ContactDetailsActivity.class);
            intent.putExtra(Contact.class.getName(), Parcels.wrap(contact));
            startActivity(intent);
        }

    }

    @Override
    public void onLongClickInteraction(Contact item) {
        deleteContact(item);
    }

    private void deleteContact(final Contact contact) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        mDatabase = FirebaseDatabase.getInstance().getReference("contacts");
                        mDatabase.child(contact.getFirebaseKey()).removeValue();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.delete_msg))
                .setMessage(contact.getName() + " " + contact.getLastname())
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();

    }

    @Override
    public void onClick(View v) {
        if (v == mAddContactBtn) {
            addNewContact();
        }
    }

    private void addNewContact() {
        Intent intent = new Intent(this, NewContactActivity.class);
        startActivity(intent);
    }
}
