package com.servicefusion.contactlist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.servicefusion.contactlist.entities.Contact;
import com.servicefusion.contactlist.fragments.NewContactFragment;

public class NewContactActivity extends FragmentActivity {

    private NewContactFragment newContactFragment;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);

        mDatabase = FirebaseDatabase.getInstance().getReference("contacts");
        newContactFragment = (NewContactFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentNewContact);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        if (item.getItemId() == R.id.menu_save) {
            Contact contact = newContactFragment.getContact();
            if (contact!=null) {
                DatabaseReference newRef = mDatabase.push();
                contact.setFirebaseKey(newRef.getKey());
                newRef.setValue(contact);
                onBackPressed();
            }
        }
        return true;
    }
}
