package com.servicefusion.contactlist.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.servicefusion.contactlist.R;
import com.servicefusion.contactlist.fragments.ContactListFragment.OnListFragmentInteractionListener;
import com.servicefusion.contactlist.entities.Contact;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactListRecycleAdapter extends RecyclerView.Adapter<ContactListRecycleAdapter.ViewHolder> {

    private List<Contact> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context mContext;

    public ContactListRecycleAdapter(List<Contact> items, OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContactName.setText(mValues.get(position).getName() + " " + mValues.get(position).getLastname());

        final String finalPhone = mValues.get(position).getPhones().get(0);
        holder.mContactPhone.setText("Phone: " + finalPhone);

        holder.mContactPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + finalPhone));
                if (PackageManager.PERMISSION_GRANTED == mContext.checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE)) {
                    mContext.startActivity(intent);
                } else {
                    Toast.makeText(mContext, "Call permission not granted", Toast.LENGTH_SHORT).show();
                }
            }
        });


        Picasso.with(mContext).load(mValues.get(position).getSmallImageURL()).placeholder(R.drawable.placeholder).into(holder.mContactImage);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onLongClickInteraction(holder.mItem);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContactName;
        public final TextView mContactPhone;
        public final ImageView mContactImage;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContactName = (TextView) view.findViewById(R.id.txtContactName);
            mContactPhone = (TextView) view.findViewById(R.id.txtPhone);
            mContactImage = (ImageView)view.findViewById(R.id.imageView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContactPhone.getText() + "'";
        }
    }

    public void setmValues(List<Contact> mValues) {
        this.mValues = mValues;
    }
}
