package com.servicefusion.contactlist.fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.servicefusion.contactlist.R;
import com.servicefusion.contactlist.Utils.AppConfiguration;
import com.servicefusion.contactlist.entities.Address;
import com.servicefusion.contactlist.entities.Contact;
import com.servicefusion.contactlist.entities.ContactDetail;
import com.servicefusion.contactlist.services.ContactService;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactDetailFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Contact mContact;
    private ImageView mImageView;
    private TextView mStreet;
    private TextView mCityStateZip;
    private TextView mWebsite;
    private TextView mEmail;

    public ContactDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactDetailFragment.
     */
    public static ContactDetailFragment newInstance(Contact contact) {
        ContactDetailFragment fragment = new ContactDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(Contact.class.getName(), Parcels.wrap(contact));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mContact = Parcels.unwrap(getArguments().getParcelable(Contact.class.getName()));
        }
        if (getActivity().getIntent().hasExtra(Contact.class.getName())) {
            mContact = Parcels.unwrap(getActivity().getIntent().getParcelableExtra(Contact.class.getName()));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_detail_fragment, container, false);

        TextView txtContactName = (TextView)view.findViewById(R.id.txtContactName);
        TextView txtLastname = (TextView)view.findViewById(R.id.txtLastname);
        TextView txtBirthdate= (TextView)view.findViewById(R.id.txtBirthdate);
        LinearLayout addressLinearGroup = (LinearLayout)view.findViewById(R.id.addressGroupLayout);
        LinearLayout phoneLinearGroup = (LinearLayout)view.findViewById(R.id.phoneGroupLayout);
        LinearLayout emailLinearGroup = (LinearLayout)view.findViewById(R.id.emailGroupLayout);


        mImageView = (ImageView)view.findViewById(R.id.imageView);
//        mStreet = (TextView)view.findViewById(R.id.txtStreet);
//        mCityStateZip = (TextView)view.findViewById(R.id.txtCityStateZip);
//        mWebsite = (TextView)view.findViewById(R.id.txtWebsite);
//        mEmail = (TextView)view.findViewById(R.id.txtEmail);

        txtContactName.setText(mContact.getName());
        txtLastname.setText(mContact.getLastname());
        txtBirthdate.setText(mContact.getBirthdate());


        List<String> addresses = mContact.getAddresses();
        if (addresses!=null) {
            (view.findViewById(R.id.line1)).setVisibility(View.VISIBLE);
            for (String address : addresses) {
                addTextView(addressLinearGroup, address, android.R.drawable.ic_menu_myplaces);
            }
        }

        List<String> phones = mContact.getPhones();
        for (String phone : phones) {
            addTextView(phoneLinearGroup, phone, android.R.drawable.stat_sys_speakerphone);
        }

        List<String> mails = mContact.getMails();
        for (String mail : mails) {
            addTextView(emailLinearGroup, mail, android.R.drawable.sym_action_email);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfiguration.ServiceURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ContactService service = retrofit.create(ContactService.class);
        Call<ContactDetail> contactDetailCall = service.getContactDetail(1);

        contactDetailCall.enqueue(new Callback<ContactDetail>() {
            @Override
            public void onResponse(Response<ContactDetail> response, Retrofit retrofit) {
                ContactDetail contactDetail1 = response.body();

                Picasso.with(getActivity()).load(contactDetail1.getLargeImageURL()).placeholder(R.drawable.placeholder).into(mImageView);

                Address address = contactDetail1.getAddress();

                mStreet.setText(address.getStreet());
                mCityStateZip.setText(address.getCity() + ", " + address.getState() + ", " + address.getZip());
                mEmail.setText(contactDetail1.getEmail());
                mWebsite.setText(contactDetail1.getWebsite());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });



        return view;

    }

    private void addTextView(LinearLayout linearLayout, String text, int icon) {
        TextView textView = new TextView(this.getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        textView.setLayoutParams(params);
        textView.setText(text);
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);

        linearLayout.addView(textView);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
