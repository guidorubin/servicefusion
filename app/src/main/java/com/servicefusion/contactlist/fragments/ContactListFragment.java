package com.servicefusion.contactlist.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.servicefusion.contactlist.R;
import com.servicefusion.contactlist.adapters.ContactListRecycleAdapter;
import com.servicefusion.contactlist.entities.Contact;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ContactListFragment extends Fragment {

    private static final String TAG = ContactListFragment.class.getName();

    private DatabaseReference mDatabase;
    private OnListFragmentInteractionListener mListener;
    private ContactListRecycleAdapter contactListRecycleAdapternew;
    private ArrayList<Contact> mContacts = new ArrayList<>();

    public ContactListFragment() {
    }

    public static ContactListFragment newInstance(int columnCount) {
        ContactListFragment fragment = new ContactListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) getActivity();
        } else {
            throw new RuntimeException(getActivity().toString()
                    + " must implement OnListFragmentInteractionListener");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_list_fragment, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            contactListRecycleAdapternew = new ContactListRecycleAdapter(mContacts, mListener, getActivity());
            recyclerView.setAdapter(contactListRecycleAdapternew);

            getContacts();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Contact item);
        void onLongClickInteraction (Contact item);
    }


    public void getContacts() {

        FirebaseDatabase.getInstance().getReference().onDisconnect();

        mDatabase = FirebaseDatabase.getInstance().getReference("contacts");

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Loading", "Wait while loading...");

                Iterable<DataSnapshot> records = dataSnapshot.getChildren();
                mContacts.clear();
                for (DataSnapshot datash : records) {
                    Contact contact = datash.getValue(Contact.class);
                    contact.setFirebaseKey(datash.getKey());
                    mContacts.add(contact);
                }

                System.out.println(dataSnapshot.getChildrenCount());
                contactListRecycleAdapternew.setmValues(mContacts);
                contactListRecycleAdapternew.notifyDataSetChanged();

                progressDialog.dismiss();
//                Log.d(TAG, "User name: " + contact.getName() + ", email " + contact.getLastname());
            }

            @Override
            public void onCancelled(DatabaseError error) {

                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


    }
}
