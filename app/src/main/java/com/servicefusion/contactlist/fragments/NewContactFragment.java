package com.servicefusion.contactlist.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.servicefusion.contactlist.R;
import com.servicefusion.contactlist.entities.Contact;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewContactFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewContactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class NewContactFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private LinearLayout mAddressGroup;
    private LinearLayout mPhonesGroup;
    private LinearLayout mEmailsGroup;
    private ArrayList<EditText> mAddresses;
    private ArrayList<EditText> mPhones;
    private ArrayList<EditText> mEmails;
    private TextWatcher addressTextWatcher;
    private TextWatcher phoneTextWatcher;
    private TextWatcher emailsTextWatcher;

    public static final String DATEPICKER_TAG = "datepicker";
    private EditText birthdateEditText;
    private EditText nameEditText;
    private EditText lastnameEditText;

    public NewContactFragment() {
        // Required empty public constructor
    }

    public static NewContactFragment newInstance() {
        NewContactFragment fragment = new NewContactFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_contact_fragment, container, false);

        birthdateEditText = (EditText)view.findViewById(R.id.birthdateEditText);
        nameEditText = (EditText)view.findViewById(R.id.nameEditText);
        lastnameEditText = (EditText)view.findViewById(R.id.lastnameEditText);

        setDatePicker();

        mAddresses = new ArrayList<EditText>();
        mPhones = new ArrayList<EditText>();
        mEmails = new ArrayList<EditText>();

        mAddressGroup = (LinearLayout)view.findViewById(R.id.addressGroupLayout);
        mPhonesGroup = (LinearLayout)view.findViewById(R.id.phoneGroupLayout);
        mEmailsGroup = (LinearLayout)view.findViewById(R.id.emailGroupLayout);

        setTextsWatcher();

        addAddress();
        addPhone();
        addEmail();

        return view;

    }

    private void setDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        birthdateEditText.setFocusable(false);
        birthdateEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                datePickerDialog.setYearRange(1902, calendar.get(Calendar.YEAR));
                datePickerDialog.setCloseOnSingleTapDay(true);
                if (!datePickerDialog.isAdded()) {
                    datePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
                }
                return false;
            }
        });

    }

    private void setTextsWatcher() {
        addressTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mAddresses.size() > 0 && s.length()>0) {
                    if (getActivity().getCurrentFocus() == mAddresses.get(mAddresses.size()-1)) {
                        addAddress();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        phoneTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mPhones.size()>0 && s.length()>0) {
                    if (getActivity().getCurrentFocus() == mPhones.get(mPhones.size()-1)) {
                        addPhone();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        emailsTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mEmails.size()>0 && s.length()>0) {
                    if (getActivity().getCurrentFocus() == mEmails.get(mEmails.size()-1)) {
                        addEmail();
                    }
                }

            }
        };

    }

    private void addPhone() {
        mPhones.add(createEditText(getString(R.string.phone_title), phoneTextWatcher, mPhonesGroup, InputType.TYPE_CLASS_PHONE));
    }

    private void addEmail() {
        mEmails.add(createEditText(getString(R.string.email_title), emailsTextWatcher, mEmailsGroup));
    }

    private void addAddress() {
        mAddresses.add(createEditText(getString(R.string.address_title), addressTextWatcher, mAddressGroup));
    }

    private EditText createEditText(String hint, TextWatcher textWatcher, LinearLayout linearLayout) {
        return createEditText(hint, textWatcher, linearLayout, InputType.TYPE_CLASS_TEXT);
    }
    private EditText createEditText(String hint, TextWatcher textWatcher, LinearLayout linearLayout, int inputType) {
        EditText editTextView = new EditText(this.getActivity());
        editTextView.setHint(hint);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        editTextView.setLayoutParams(params);
        editTextView.addTextChangedListener(textWatcher);
        editTextView.setInputType(inputType);
        linearLayout.addView(editTextView);
        return editTextView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        birthdateEditText.setText(month + "/" + day + "/" + year);
        mAddressGroup.requestFocus();

    }


    public Contact getContact() {
        Contact contact =  new Contact();
        if (nameEditText.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), getString(R.string.name_prompt), Toast.LENGTH_SHORT).show();;
            return null;
        }
        contact.setBirthdate(birthdateEditText.getText().toString());
        contact.setName(nameEditText.getText().toString());
        contact.setLastname(lastnameEditText.getText().toString());

        ArrayList<String> list = new ArrayList<>();
        for (EditText editText : mAddresses) {
            if (editText.getText().length()>0) {
                list.add(editText.getText().toString());
            }
        }
        contact.setAddresses(list);

        list = new ArrayList<>();
        for (EditText editText : mPhones) {
            if (editText.getText().length()>0) {
                list.add(editText.getText().toString());
            }
        }
        if(list.size()==0) {
            Toast.makeText(getActivity(), getString(R.string.phone_prompt), Toast.LENGTH_SHORT).show();;
            return null;
        }
        contact.setPhones(list);

        list = new ArrayList<>();
        for (EditText editText : mEmails) {
            if (editText.getText().length()>0) {
                list.add(editText.getText().toString());
            }
        }
        if(list.size()==0) {
            Toast.makeText(getActivity(), getString(R.string.email_prompt), Toast.LENGTH_SHORT).show();;
            return null;
        }

        contact.setMails(list);
        return contact;

    }
}
