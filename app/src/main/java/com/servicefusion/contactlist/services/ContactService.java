package com.servicefusion.contactlist.services;

import com.servicefusion.contactlist.entities.Contact;
import com.servicefusion.contactlist.entities.ContactDetail;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Guid on 06/01/2016.
 */
public interface ContactService {

    @GET("/external/contacts.json")
    Call<ArrayList<Contact>> getContacts();

    @GET("/external/Contacts/id/{id}.json")
    Call<ContactDetail> getContactDetail(@Path("id") int contactId);
}
